FROM openjdk:8-jre-alpine
ADD target/hello-springboot-docker-0.1.0.jar app.jar
ENV JAVA_OPTS=""
CMD [ "sh", "-c", "java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /app.jar" ]
