package hello;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class Application {

	Logger LOGGER = Logger.getLogger(this.getClass().getName());
	
    @RequestMapping("/")
    public String home(HttpServletRequest request) {

    	String hostIP = "undefined";

    	try {
			hostIP = InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			LOGGER.log(Level.SEVERE, "Error getting IP address.", e);
		}

    	LOGGER.log(Level.INFO, "Received request from " + request.getRemoteAddr() + " to " +  hostIP);

    	return "Hello World from " +  hostIP;

    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
